const $ = (element) => document.querySelector(element);

const sayHello = () => {
  const input = $('input') 
  const name = input.value;
  const p = $('p');
  
  p.innerHTML = `Hello, ${name}`;
  input.value = '';
}

$('button')
  .addEventListener('click', sayHello);

